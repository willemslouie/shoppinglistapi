﻿using Microsoft.EntityFrameworkCore;

namespace ShoppinglistAPI.Entities
{
    public class ProductContext : DbContext
    {
        public ProductContext(DbContextOptions<ProductContext> options)
            :base(options)
        {
            //de database wordt zeker gecreeerd moest deze nog niet bestaan
            Database.EnsureCreated();
        }
            // products is going to become de table met de rijen van classe product
         public DbSet<Product> products { get; set; }
    }
}
