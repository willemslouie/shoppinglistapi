﻿using System.ComponentModel.DataAnnotations;

namespace ShoppinglistAPI.Entities
{
    public class Product
    {   
        public int Id { get; set; }
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }
        [Required]
        public bool Checked { get; set; }
    }
}
