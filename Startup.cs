﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Cors.Internal;
using Microsoft.Extensions.DependencyInjection;
using ShoppinglistAPI.Entities;
using Microsoft.EntityFrameworkCore;

namespace ShoppinglistAPI
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            // connectable from other localhost
            services.AddCors(o => o.AddPolicy("MyPolicy", builder =>
            {
                builder.AllowAnyOrigin()
                       .AllowAnyMethod()
                       .AllowAnyHeader();
            }));
            services.AddMvc();
            services.Configure<MvcOptions>(options =>
            {
                options.Filters.Add(new CorsAuthorizationFilterFactory("MyPolicy"));
            });
            // database
            services.AddDbContext<ProductContext>(options =>
            {
                //shoppinglist:name of database
                var connect = @"Server=localhost,1433;Database=shoppinglistDB;persist security info=True;Trusted_Connection=False;User=SA;password=reallyStrongPwd123;";
                options.UseSqlServer(connect);
                //options.UseInMemoryDatabase("shoppingDB");
            });
        }
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseCors("MyPolicy");
            app.UseMvc();
            app.UseStatusCodePages();

            app.Run(async (context) =>
            {
                await context.Response.WriteAsync("");
            });
        }
    }
}
