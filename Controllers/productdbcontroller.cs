﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using ShoppinglistAPI.Entities;

namespace ShoppinglistAPI.Controllers
{
    [Route("api/products")]
    [ApiController]
    public class productdbcontroller : Controller
    {
        private readonly ProductContext _context;
        public productdbcontroller(ProductContext context)
        {
            _context = context;
        }

        [HttpGet()]
        public ActionResult<List<Product>> GetAll()
        {
            return _context.products.ToList();
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var item = _context.products.Find(id);
            if (item == null) return NotFound();
            return Ok(item);
        }

        [HttpPost()]
        public IActionResult PostCreateProduct([FromBody] Product create)
        {
            if (create == null) return BadRequest();
            _context.products.Add(create);
            _context.SaveChanges();
            return Ok();
        }

        [HttpPut("{id}")]
        public IActionResult UpdateProduct(int id, [FromBody] Product update)
        {
            var productReturn = _context.products.Find(id);
            if (productReturn == null) return NotFound();
            if (update == null) return BadRequest();
            productReturn.Name = update.Name;
            productReturn.Checked = update.Checked;
           
            _context.SaveChanges();
            return Ok();
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteProduct(int id)
        {
            var productReturn = _context.products.Find(id);
            if (productReturn == null) return NotFound();
            _context.products.Remove(productReturn);
            _context.SaveChanges();
            return Ok("Product " + productReturn + ", at index [" + id + "] is deleted");
        }
    }
}
